package ru.anleon;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(1000);
                return "Hello world!";
            }
        });

        while (!future.isDone()) {
            System.out.println("inProgress");
            System.out.println(future.get());
        }


        executorService.shutdown();

    }
}
