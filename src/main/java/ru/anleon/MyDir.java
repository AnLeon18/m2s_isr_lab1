package ru.anleon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyDir implements MyFile {


    private final String name;
    private List<MyFile> myFiles = new ArrayList<>();

    public MyDir(File[] files, String name) {
        for (File file : files) {
            myFiles.addAll(new Sprinter().getList(file));
        }
        this.name = name;
    }

    @Override
    public String print(String indent) {

        StringBuilder str = new StringBuilder(indent + "*"+ name + "\n");
        String newIndent = indent + "\t";
        for (MyFile myFile : myFiles) {
            str.append(myFile.print(newIndent));
        }
        return str.toString();
    }
}
