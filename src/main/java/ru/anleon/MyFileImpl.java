package ru.anleon;

import java.io.File;

public class MyFileImpl implements MyFile {

    private String name;

    public MyFileImpl(File rootDir) {
        this.name = rootDir.getName();
    }

    @Override
    public String print(String indent) {
        return  indent + name + "\n";
    }
}
