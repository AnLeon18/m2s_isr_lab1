package ru.anleon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Sprinter {

    public List<MyFile> getList(File rootDir) {
        List<MyFile> myFiles = new ArrayList<>();

        if (rootDir.isDirectory()) {
            File[] files = rootDir.listFiles();
            MyFile myFile = files != null ? new MyDir(files, rootDir.getName()) : new MyFileImpl(rootDir);
            myFiles.add(myFile);
        } else {
            myFiles.add(new MyFileImpl(rootDir));
        }

        return myFiles;
    }
}
