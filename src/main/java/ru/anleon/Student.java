package ru.anleon;

import java.util.Date;

public class Student implements Comparable<Student> {
    private String name;
    private String lastName;
    private Date date;
    private String zk;

    public Student(String name, String lastName) {
        this(name, lastName, null, null);
    }

    public Student(String name, String lastName, Date date, String zk) {
        this.name = name;
        this.lastName = lastName;
        this.date = date;
        this.zk = zk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getZk() {
        return zk;
    }

    public void setZk(String zk) {
        this.zk = zk;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public int compareTo(Student student) {
        return toString().compareTo(student.toString());
    }
}
