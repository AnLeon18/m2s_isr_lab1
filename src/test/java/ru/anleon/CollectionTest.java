package ru.anleon;

import org.junit.Test;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CollectionTest {

    static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    @Test
    public void test() {

        new Student("Andrey", "popov");

        List<Student> students = new ArrayList<>();


        try {
            students.add(new Student("first", "last", sdf.parse("30.01.1998"), "sd13223"));
            students.add(new Student("date", "date", sdf.parse("30.01.1999"), "sad1e12e"));
            students.add(new Student("date2", "date2", sdf.parse("30.01.1999"), "dsad1d1213"));
            students.add(new Student("date", "date", sdf.parse("30.01.1999"), "ad1dw4sad14"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Collections.sort(students);

        System.out.println(students);

        Collections.sort(students, Comparator.comparing(Student::getDate));

        System.out.println(students);


        Map<String, Student> studentMap = new HashMap<>();
        students.forEach(student -> studentMap.put(student.getZk(), student));
        System.out.println(studentMap);


    }

    @Test
    public void disk() {

        File rootDir = new File("/home/andrey/git/m2s_isr_lab1");

        List<MyFile> myFiles = new Sprinter().getList(rootDir);

        myFiles.forEach(myFile -> System.out.println(myFile.print("")));
    }
}
